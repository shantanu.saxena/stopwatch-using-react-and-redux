import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Start } from "../Action/TimerAction";
import "./css/Button.css";

const StartButton = () => {
  let dispatch = useDispatch();
  let { isStarted } = useSelector((state) => state);
  const handleClick = () => {
    dispatch(Start());
  };

  return (
    <>
      {!isStarted ? (
        <button
          className="startBtn btn"
          onClick={() => {
            handleClick();
          }}
        >
          Start
        </button>
      ) : (
        ""
      )}
    </>
  );
};

export default StartButton;
