import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Pause } from "../Action/TimerAction";
import "./css/Button.css";

const StopButton = () => {
  let dispatch = useDispatch();
  let { pause, isStarted } = useSelector((state) => state);
  const handleClick = () => {
    dispatch(Pause());
    let StopBtn = document.querySelector(".StopBtn");
  };
  return (
    <>
      {isStarted ? (
        <button
          className="StopBtn btn"
          onClick={() => {
            handleClick();
          }}
        >
          {pause ? "Resume" : "Stop"}
        </button>
      ) : (
        ""
      )}
    </>
  );
};

export default StopButton;
