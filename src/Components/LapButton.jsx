import React from "react";
import { useSelector, useDispatch } from "react-redux";
import "./css/Button.css";

const LapButton = () => {
  let dispatch = useDispatch();
  let { pause, isStarted } = useSelector((state) => state);
  let { hr, min, sec, milliSec, laps } = useSelector((state) => state);
  let lapData = {
    num: laps.length + 1,
    hr,
    min,
    sec,
    milliSec,
  };

  const handleClick = () => {
    dispatch({ type: "ADD_LAP", payload: lapData });
  };

  return (
    <>
      {isStarted && !pause ? (
        <button className="LapBtn btn"
          onClick={() => {
            handleClick();
          }}
        >
          lap
        </button>
      ) : (
        ""
      )}
    </>
  );
};

export default LapButton;
